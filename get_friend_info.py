import pickle

import tweepy

from tqdm import tqdm


consumer_key = 'sS25OzYp60Ghi81D5Jwx8KFiC'
consumer_secret = 'Z4wTkRCs2Yx31r6YWtsHbs1GEE08NChHGWndDAsKNyCJ7I4uVT'

access_token = '758106823786242048-qZK96DAGsHohztTieWyWHsJsRBBzJnf'
access_token_secret = 'AvqkYvAnPpoLqSgLgWrNAfqEPHzfrYIlLZZBDlgbd5zq5'


auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)


with open('friends.txt') as f:
    friends = [name.rstrip() for name in f.readlines()]

def get_info(friend):
	try:
		return api.get_user(friend)
	except tweepy.error.TweepError:
		print('error:', friend)

friend_info = [get_info(friend) for friend in tqdm(friends)]

friend = friend_info[0]

friends_info_cooked = [
    {'screen_name': f.screen_name, 
     'name': f.name,
     'profile_image_url': f.profile_image_url,
     'profile_image_large_url': f.profile_image_url.replace('_normal', '_400x400')}
    for f in friend_info
]

with open('../friends_info.pkl', 'wb') as f:
    pickle.dump(friends_info_cooked, f)
